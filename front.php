<!DOCTYPE html>
<html>
<head>
	   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

	<title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/mdb.css" rel="stylesheet">
    <link href="css/compile.min.css" rel="stylesheet">

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js">
</script>

<script>
	//Material select
	$(document).ready(function() {
    	$('.mdb-select').material_select();
  	});
</script>
<script>
    $(document).ready(function () {
  $('body').on('click', '#selectAll', function () {
    if ($(this).hasClass('allChecked')) {
        $('input[type="checkbox"]', '#example').prop('checked', false);
    } else {
        $('input[type="checkbox"]', '#example').prop('checked', true);
    }
    $(this).toggleClass('allChecked');
  })
});
</script>

</head>
<body>
	
		
<div style="width:100%;height:100px;background-color:#575850;" class="container-fluid">
	<p style="color:white;font-size:50px;margin-top:20px;margin-left: 20px;">Exam preparation online</p>
</div>
<p style="margin-left: 200px;color: red;font-size: 20px;">
<?php
session_start();
if(isset($_SESSION['$msg']))
{
	$msg=$_SESSION['$msg'];

	echo $msg;
	 unset($_SESSION['$msg']);
}
?>
</p>
<form class="form-inline" action="code_exec.php" method="POST">
<div style="width: 100%; margin-top: 20px;margin-left: 20px;">
    <div class="float-left" style="height: 300px;width:350px;background-color: #CCD3FF;">
    	<p style="font-size: 20px;margin-left: 20px;margin-top: 20px;">Engineering Stream:</p>
    	<div style="margin-top: 10px;margin-left: 20px; width: 200px;font-size: 20px;">
    		
										<select name="stream" class="mdb-select"  style="width: 250px;height:40px;font-size: 20px;">

									        <option value="cse">CSE</option>
											<option value="ece">ECE</option>
											<option value="it">IT</option>
											<option value="Mechanical">Mechanical</option>
											<option value="chemical Engineering">chemical enginnering</option>
										</select>
										<!--label>Stream</label-->
									</div>
	<p style="font-size: 20px;margin-left: 20px;">Application mode:</p>
										<div style="margin-top: 10px;margin-left: 20px;width:200px;font-size: 20px;">
    	
										<select name="mode" id ="mode" class="mdb-select" >
											
											<option value="on campus">ON CAMPUS</option>
											<option value="off campus">OFF CAMPUS</option>
							
										</select>
									</div>


    </div>
    <div class="float-right" style="margin-right: 300px;width:700px;" >
    	<p style="font-size: 40px;">INTERVIEW EXPRIENCE :: ARW</p>

    	<textarea class="md-textarea" placeholder="write something here" style="font-size: 20px;" name="about"></textarea>
    </div>
    </div> 
    <div  style="background-color:#E0D1FA;height: 300px;width:350px;margin-left: 20px;margin-top: 20px;">
       <button type="button" id="selectAll" style="border:none;text-decoration: underline;margin-left:230px;">select all</button> 
       <div id="example">
   <div class="form-group" style="margin-top:30px;margin-left: 50px">
        <input type="checkbox" id="checkbox11" name="check11" value="on">
        <label for="checkbox11">Technical interview</label>
    </div>
    <div class="form-group" style="margin-top:30px;margin-left: 50px">
        <input type="checkbox" id="checkbox13" name="check13" value="on">
        <label for="checkbox13">HR QUESTIONS</label>
    </div>
</div>

  </div>
  <div class="float-right" style="background-color: #B8FAFF;margin-left:170px;width:800px;height: 100px">
  <button type="submit" name="submit" class="btn btn-default waves-effect waves-light" style="margin-left: 650px;margin-top: 30px">Submit</button>
</div>
  </form>



</body>
</html>